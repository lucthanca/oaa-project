<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    public function index()
    {
        $products = Product::orderBy('created_at', 'desc')
            ->get()
            ->take(18);
        $chunkProducts = $products
            ->chunk(2);
        // foreach ($products as $product) {
        //     dd($product->productimages()->where('status', 1)->orderBy('created_at', 'desc')->take(1)->get());
        // }
        // dd('srop');
        $session = Session::has('cart') ? Session::get('cart') : null;
        $topSale = Product::all();
        // foreach ($topSale as $product) {
        //     if ($product->orders->isEmpty()) {
        //         echo 'empty<br>';
        //     } else {
        //         dd($product->orders()->count());
        //     }
        // }

        return view('frontend.home.index', compact('products', 'chunkProducts', 'session', 'topSale'));
    }
}
