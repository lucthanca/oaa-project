$(document).ready(function () {
    var cloned = $('.owl-stage').children('.cloned');

    $(document).on('click', '.add_cart_btn', function () {

        var productQtyInput = $(this).prev().children('input').val();
        var id = $(this).attr('data-id');
        // Nếu thêm vào giỏ ở trên trang thông tin sản phẩm
        console.log(productQtyInput);
        if (productQtyInput == undefined) {
            $.ajax({
                type: "get",
                url: "/addToCard/" + id,
                data: {},
                dataType: "json",
                success: function (data) {
                    console.log(data);
                    const Toast = Swal.mixin({
                        toast: true,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 5000
                    });
                    Toast.fire({
                        type: 'success',
                        title: 'Thêm vào giỏ hàng thành công :)'
                    });

                    loadCart();

                },
                error: function (data) {
                    var er = data.responseJSON;
                    console.log(er);
                }
            });
        } else if (productQtyInput == 0 || isNaN(parseInt(productQtyInput))) {
            Swal.fire({
                title: 'Hãy chọn số lượng hợp lệ nhoé !',
                type: 'info',
                animation: false,
                customClass: {
                    popup: 'animated tada'
                },
            });
        } else {
            $.ajax({
                type: "get",
                url: "/addToCard/" + id,
                data: {
                    "qty": parseInt(productQtyInput),
                },
                dataType: "json",
                success: function (data) {
                    console.log(data);
                    const Toast = Swal.mixin({
                        toast: true,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 5000
                    });
                    Toast.fire({
                        type: 'success',
                        title: 'Thêm vào giỏ hàng thành công :)'
                    });
                    loadCart();
                },
                error: function (data) {
                    var er = data.responseJSON;
                    console.log(er);
                }
            });
        }
    });
});

function loadCart() {
    $.ajax({
        type: "get",
        url: "/getTotalQty",
        data: {},
        dataType: "json",
        success: function (data) {
            var dataContent = data.totalQty;
            $('a.cart').attr('data-content', dataContent);
            $('a.cart').attr('title', 'Có ' + dataContent + ' sản phẩm trong giỏ hàng của bạn');

        }
    });
}
