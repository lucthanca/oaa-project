<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="{{ asset('frontend/js/jquery-3.2.1.min.js') }}"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="{{ asset('frontend/js/popper.min.js') }}"></script>
<script src="{{ asset('frontend/js/bootstrap.min.js') }}"></script>
<!-- Rev slider js -->
<script src="{{ asset('frontend/vendors/revolution/js/jquery.themepunch.tools.min.js') }}"></script>
<script src="{{ asset('frontend/vendors/revolution/js/jquery.themepunch.revolution.min.js') }}"></script>
<script src="{{ asset('frontend/vendors/revolution/js/extensions/revolution.extension.actions.min.js') }}"></script>
<script src="{{ asset('frontend/vendors/revolution/js/extensions/revolution.extension.video.min.js') }}"></script>
<script src="{{ asset('frontend/vendors/revolution/js/extensions/revolution.extension.slideanims.min.js') }}"></script>
<script src="{{ asset('frontend/vendors/revolution/js/extensions/revolution.extension.layeranimation.min.js') }}">
</script>
<script src="{{ asset('frontend/vendors/revolution/js/extensions/revolution.extension.navigation.min.js') }}"></script>
<script src="{{ asset('frontend/vendors/revolution/js/extensions/revolution.extension.slideanims.min.js') }}"></script>
<!-- Extra plugin css -->
<script src="{{ asset('frontend/vendors/counterup/jquery.waypoints.min.js') }}"></script>
<script src="{{ asset('frontend/vendors/counterup/jquery.counterup.min.js') }}"></script>
<script src="{{ asset('frontend/vendors/owl-carousel/owl.carousel.min.js') }}"></script>
<script src="{{ asset('frontend/vendors/bootstrap-selector/js/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('frontend/vendors/image-dropdown/jquery.dd.min.js') }}"></script>
<!-- <script src="{{ asset('frontend/js/smoothscroll.js') }}"></script> -->
<script src="{{ asset('frontend/vendors/isotope/imagesloaded.pkgd.min.js') }}"></script>
<script src="{{ asset('frontend/vendors/isotope/isotope.pkgd.min.js') }}"></script>
<script src="{{ asset('frontend/vendors/magnify-popup/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ asset('frontend/vendors/vertical-slider/js/jQuery.verticalCarousel.js') }}"></script>
<script src="{{ asset('frontend/js/theme.js') }}"></script>
<script src="{{ asset('frontend/js/sweetalert2.all.min.js') }}"></script>
<script src="{{ asset('frontend/js/my.js') }}"></script>
<script>
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
</script>
@yield('js')
