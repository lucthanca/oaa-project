@extends('layouts.partions.frontend.master')
@section('title')
Oaaaaaaaaa
@endsection

@section('css')


@endsection

@section('js')



<script>
$(document).ready(function() {
    $(document).on('click', '._remove', function() {
        // Xoá sp khỏi giỏ hàng
        Swal.fire({
            title: 'Nhắc nhẹ !',
            text: "Bạn có muốn xoá sản phẩm này khỏi giỏ hàng?!",
            type: 'warning',
            animation: false,
            customClass: {
                popup: 'animated tada'
            },
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ừm !',
            cancelButtonText: 'Không, mình ấn nhầm đấy'
        }).then((result) => {
            if (result.value) {
                // Lấy id sản phẩm
                var productId = $(this).attr('data-id');
                // Lấy ra đối tượng cần xoá trực tiếp trên browser
                var thisRemove = $(this);
                $.ajax({
                    type: "post",
                    url: "/cart/remove",
                    data: {
                        id: productId,
                    },
                    dataType: "json",
                    success: function(data) {
                        // Nếu xoá hết trong giỏ hàng thì trả về giao diện
                        if (data.cart == 'empty') {
                            window.location = '/cart';
                        } else {
                            loadCart();
                            // Load lại tổng tiền
                            $('.total-price').text(parseFloat($('.total-price')
                                .text()) - parseFloat(thisRemove.parent()
                                .next().next().next().next().children()
                                .text()));
                            $('.total-amount').text(parseFloat($('.total-price')
                                .text()) + 30000);
                            // Xoá sp trên trình duyệt
                            console.log(thisRemove.parent().parent().remove());

                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 5000
                            });
                            Toast.fire({
                                type: 'success',
                                title: 'Xoá sản phẩm thành công :)'
                            });
                        }
                    },
                    error: function(data) {
                        var er = data.responseJSON;
                        console.log(er);
                    }
                });
            }
        });
    });

    // Thêm số lượng
    $(document).on('click', '#_plus', function() {
        // Lấy id sp
        var id = $(this).attr('data-id');
        var thisAdd = $(this);
        console.log(id);
        $.ajax({
            type: "get",
            url: "/addToCard/" + id,
            data: {},
            dataType: "json",
            success: function(data) {
                loadCart();
                // Load lại tổng tiền
                $('.total-price').text(parseFloat($('.total-price')
                    .text()) + parseFloat(thisAdd.parent().parent().parent().prev().children().text()));
                $('.total-amount').text(parseFloat($('.total-price')
                    .text()) + 30000);
                    thisAdd.parent().parent().parent().next().children().text(thisAdd.prev().val() * parseFloat(thisAdd.parent().parent().parent().prev().children().text()));
            }
        });
    });

    // Xoá số lượng
    $(document).on('click', '#_minus', function () {
        // Lấy id sp
        var id = $(this).attr('data-id');
        var thisMinus = $(this);
        console.log(id);
        $.ajax({
            type: "post",
            url: route('minusA_Product'),
            data: {
                "id": id,
            },
            dataType: "json",
            success: function (data) {
                loadCart();
                // Load lại tổng tiền
                $('.total-price').text(parseFloat($('.total-price')
                    .text()) - parseFloat(thisMinus.parent().parent().parent().prev().children().text()));
                $('.total-amount').text(parseFloat($('.total-price')
                    .text()) + 30000);
                    thisMinus.parent().parent().parent().next().children().text(thisMinus.next().val() * parseFloat(thisMinus.parent().parent().parent().prev().children().text()));
            }
        });
    });
});
</script>

@endsection


@section('content')

<!--================Categories Banner Area =================-->
<section class="solid_banner_area">
    <div class="container">
        <div class="solid_banner_inner">
            <h3>Giỏ hàng của bạn</h3>
            <ul>
                <li><a href="#">Trang chủ</a></li>
                <li><a href="shopping-cart2.html">Giỏ hàng</a></li>
            </ul>
        </div>
    </div>
</section>
<!--================End Categories Banner Area =================-->


<!--================Shopping Cart Area =================-->
<section class="shopping_cart_area p_100">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="cart_items">
                    <h3>Sản phẩm trong giỏ</h3>
                    <div class="table-responsive-md">
                        <table class="table">
                            <tbody>
                                @if($products)
                                @foreach($products as $product)

                                <tr>
                                    <th scope="row">
                                        <a href="javascript:void(0);" class="_remove"
                                            data-id="{{ $product['item']->id }}" title="Xoá khỏi giỏ hàng">
                                            <img src="{{ asset('frontend/img/close-icon.png') }}" alt="xoá"
                                                style="cursor: pointer;">
                                        </a>
                                    </th>
                                    <td>
                                        <div class="media">
                                            <div class="d-flex">
                                                <a href="san-pham/{{ $product['item']->id }}">
                                                    <img src="{{ $product['item']->getImage() }}"
                                                        alt="{{ $product['item']->name }}"
                                                        style="max-width: 64px;max-height: 64px;">
                                                </a>
                                            </div>
                                            <div class="media-body">
                                                <a href="san-pham/{{ $product['item']->id }}">
                                                    <h4>{{ $product['item']->name }}</h4>
                                                </a>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <p class="red">{{ $product['item']->price }}</p>
                                    </td>
                                    <td>
                                        <div class="quantity">
                                            <h6>Số lượng</h6>
                                            <div class="custom">
                                                <button id="_minus" data-id="{{ $product['item']->id }}"
                                                    onclick="var result = document.getElementById('sst'); var sst = result.value; if( !isNaN( sst ) &amp;&amp; sst > 0 ) result.value--;return false;"
                                                    class="reduced items-count" type="button"><i
                                                        class="icon_minus-06"></i></button>
                                                <input readonly type="text" name="qty" id="sst" maxlength="12"
                                                    value="{{ $product['qty'] }}" title="Quantity:"
                                                    class="input-text qty" style="background: #f0f0f0;">
                                                <button id="_plus" data-id="{{ $product['item']->id }}"
                                                    onclick="var result = document.getElementById('sst'); var sst = result.value; if( !isNaN( sst )) result.value++;return false;"
                                                    class="increase items-count" type="button"><i
                                                        class="icon_plus"></i></button>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <p>{{ $product['price'] }}</p>
                                    </td>
                                </tr>

                                @endforeach
                                @endif
                                <!-- Empty row -->
                                <tr>
                                    <th scope="row">
                                    </th>
                                </tr>
                                <!-- End empty row -->
                                <tr class="last">
                                    <th scope="row">
                                        <img src="{{ asset('frontend/img/cart-icon.png') }}" alt="">
                                    </th>
                                    <td>
                                        <div class="media">
                                            <div class="d-flex">
                                                <h5>Mã giảm giá</h5>
                                            </div>
                                            <div class="media-body">
                                                <input type="text" placeholder="Apply cuopon">
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <p class="red"></p>
                                    </td>
                                    <td>
                                        <h3>cập nhật</h3>
                                    </td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="cart_totals_area">
                    <h4>Tổng quan lại</h4>
                    <div class="cart_t_list">
                        <div class="media">
                            <div class="d-flex">
                                <h5>Tổng tiền</h5>
                            </div>
                            <div class="media-body d-flex">
                                <h6 class="total-price">{{ $session->totalPrice }}</h6>
                                <h6>&nbsp;vnđ</h6>
                            </div>
                        </div>
                        <div class="media">
                            <div class="d-flex">
                                <h5>Giao hàng: </h5>
                            </div>
                            <div class="media-body">
                                <p>30.000đ trên toàn quốc ! :)</p>
                            </div>
                        </div>

                    </div>
                    <div class="total_amount row m0 row_disable">
                        <div class="float-left">
                            Tổng tiền thanh toán
                        </div>
                        <div class="float-right d-flex">
                            <h6 class="total-amount">{{ $session->totalPrice + 30000}}</h6>
                            <h6>&nbsp;vnđ</h6>
                        </div>
                    </div>
                </div>
                <a href="/checkout" value="submit" class="btn subs_btn form-control">Tiến hành đặt hàng</a>
            </div>
        </div>
    </div>
</section>
<!--================End Shopping Cart Area =================-->

@endsection
